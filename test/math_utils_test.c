#include "../math_utils.h"

#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define PRINT_ERR(expected_value, current_value)                         \
  printf("%s : %d : Expected value: %d | current value: %d\n", __func__, \
         __LINE__, (int)expected_value, (int)current_value)

///@brief Maximum error tolerance in ppm
#define DIFF_TOLERANCE (1)

/**
 * @brief Basic test of linear approximation with hard-coded values
 * @return Number of errors/error code
 */
int32_t mut_linear_approx_float_hardcoded(void) {
  int32_t i32_num_of_problems = 0;

  // Example ADC values for range 3.6 ~ 3.7 V
  float f_ideal_min = 708;
  float f_ideal_max = 728;

  float f_real_min = 716;
  float f_real_max = 753;

  float f_coef_offset, f_coef_mul = 0;

  uint32_t u32_corrected_value;

  mu_linear_approx_get_coef_float(f_ideal_min, f_ideal_max, f_real_min, f_real_max,
                           &f_coef_offset, &f_coef_mul);
  // Now we have coefficients, so let's some calculation begins!
  // Note that float can not be compared "if(float1 == float2)" since
  // there are some inaccuracy which might cause deciding that floats
  // are not exactly same, but practically they're same -> need to
  // convert to integers

  // Minimum value
  u32_corrected_value = (uint32_t)mu_linear_approx_get_corrected_value_float(
      f_real_min, f_coef_offset, f_coef_mul);
  if (u32_corrected_value != (uint32_t)f_ideal_min) {
    PRINT_ERR(f_ideal_min, u32_corrected_value);
    i32_num_of_problems++;
  }

  // Maximum value
  u32_corrected_value = (uint32_t)mu_linear_approx_get_corrected_value_float(
      f_real_max, f_coef_offset, f_coef_mul);

  if (u32_corrected_value != f_ideal_max) {
    PRINT_ERR(f_ideal_max, u32_corrected_value);
    i32_num_of_problems++;
  }

  // Something in the middle - manually calculated values
  u32_corrected_value =
      (uint32_t)mu_linear_approx_get_corrected_value_float(735, f_coef_offset, f_coef_mul);

  if (u32_corrected_value != 718) {
    PRINT_ERR(718, u32_corrected_value);
    i32_num_of_problems++;
  }

  u32_corrected_value =
      (uint32_t)mu_linear_approx_get_corrected_value_float(742, f_coef_offset, f_coef_mul);

  if (u32_corrected_value != 722) {
    PRINT_ERR(722, u32_corrected_value);
    i32_num_of_problems++;
  }

  if (i32_num_of_problems == 0) {
    printf("%s : No problems detected\n", __func__);
  } else {
    printf("%s : %d problems detected\n", __func__, i32_num_of_problems);
  }

  return i32_num_of_problems;
}

/**
 * @brief Basic test of linear approximation with hard-coded values
 * @return Number of errors/error code
 */
int32_t mut_linear_approx_u16_hardcoded(void) {
  int32_t i32_num_of_problems = 0;

  // Example values for range 3.6 ~ 3.7 V
  uint16_t u16_ideal_min = 708;
  uint16_t u16_ideal_max = 728;

  uint16_t u16_real_min = 716;
  uint16_t u16_real_max = 753;

  int16_t i16_coef_offset = 0;
  int32_t i32_coef_mul = 0;

  uint16_t u16_corrected_value;

  mu_linear_approx_get_coef_u16(u16_ideal_min, u16_ideal_max, u16_real_min, u16_real_max,
                         &i16_coef_offset, &i32_coef_mul);

  // Now we have coefficients, let's do some test drive!

  // Minimum value test
  u16_corrected_value =
      mu_linear_approx_get_corrected_value_u16(u16_real_min, i16_coef_offset, i32_coef_mul);

  if (u16_corrected_value != u16_ideal_min) {
    PRINT_ERR(u16_ideal_min, u16_corrected_value);
    i32_num_of_problems++;
  }

  // Maximum value
  u16_corrected_value =
      mu_linear_approx_get_corrected_value_u16(u16_real_max, i16_coef_offset, i32_coef_mul);

  if (u16_corrected_value != u16_ideal_max) {
    PRINT_ERR(u16_ideal_max, u16_corrected_value);
    i32_num_of_problems++;
  }

  // Something between - manually calculated values
  u16_corrected_value =
      mu_linear_approx_get_corrected_value_u16(735, i16_coef_offset, i32_coef_mul);

  // Due to inaccuracy there is difference +1 from ideal line
  if (u16_corrected_value != 719) {
    PRINT_ERR(719, u16_corrected_value);
    i32_num_of_problems++;
  }

  u16_corrected_value =
      mu_linear_approx_get_corrected_value_u16(742, i16_coef_offset, i32_coef_mul);

  if (u16_corrected_value != 723) {
    PRINT_ERR(723, u16_corrected_value);
    i32_num_of_problems++;
  }

  if (i32_num_of_problems == 0) {
    printf("%s : No problems detected\n", __func__);
  } else {
    printf("%s : %d problems detected\n", __func__, i32_num_of_problems);
  }

  return i32_num_of_problems;
}

int main(void) {
  int32_t i32_num_of_problems = 0;

  i32_num_of_problems += mut_linear_approx_float_hardcoded();
  i32_num_of_problems += mut_linear_approx_u16_hardcoded();

  return (int)i32_num_of_problems;
}

/**
 * @file
 * @author Martin Stejskal
 * @brief Extra math functionality which might be handy
 */
// ===============================| Includes |================================
#include "math_utils.h"

#include <assert.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
void mu_linear_approx_get_coef_float(float f_ideal_min, float f_ideal_max,
                                     float f_real_min, float f_real_max,
                                     float *pf_coef_offset,
                                     float *pf_coef_mul) {
  // Pointers should not be empty
  assert(pf_coef_mul);
  assert(pf_coef_offset);

  // This is simple math with 2 equations of 2 unknowns
  *pf_coef_mul = (f_ideal_max - f_ideal_min) / (f_real_max - f_real_min);
  *pf_coef_offset = f_ideal_min - (f_real_min * (*pf_coef_mul));
}

float mu_linear_approx_get_corrected_value_float(float f_real_value,
                                                 float f_coef_offset,
                                                 float f_coef_mul) {
  return f_coef_offset + (f_real_value * f_coef_mul);
}

void mu_linear_approx_get_coef_u16(uint16_t u16_ideal_min,
                                   uint16_t u16_ideal_max,
                                   uint16_t u16_real_min, uint16_t u16_real_max,
                                   int16_t *pi16_coef_offset,
                                   int32_t *pi32_coef_mul) {
  // Pointers should not be empty
  assert(pi16_coef_offset);
  assert(pi32_coef_mul);

  // Temporary variable for correct calculation coefficient offset. Because
  // there are multiplications and other stuff which could cause overflow
  // with 16 bit long number
  int32_t i32_coef_offset;

  // Similar to the linearApproxGetCoefFloat(). But in order to not lost
  // accuracy, coefficient multiplication is 10 000x bigger (everything below
  // 1 is 0 in integer logic)

  int32_t i32_tmp = (10000 * (int32_t)(u16_ideal_max - u16_ideal_min));
  int32_t i32_tmp2 = (int32_t)(u16_real_max - u16_real_min);

  // Handle division by zero
  if (i32_tmp2 == 0) {
    i32_tmp = 0;
  } else {
    i32_tmp = i32_tmp / i32_tmp2;
  }
  *pi32_coef_mul = i32_tmp;

  i32_coef_offset = (int32_t)u16_ideal_min -
                    (((int32_t)u16_real_min * (*pi32_coef_mul)) / 10000);

  // Should fit into 16 bit signed value
  assert((i32_coef_offset > INT16_MIN) && (i32_coef_offset < INT16_MAX));

  *pi16_coef_offset = (int16_t)i32_coef_offset;
}

uint16_t mu_linear_approx_get_corrected_value_u16(uint16_t u16_real_value,
                                                  int16_t i16_coef_offset,
                                                  int32_t i32_coef_mul) {
  // Deal with calculation as 32 bit integer
  int32_t i32_corrected_value =
      (int32_t)i16_coef_offset +
      ((int32_t)u16_real_value * i32_coef_mul) / 10000;

  // Should fit into 16 bit unsigned value
  assert(i32_corrected_value < UINT16_MAX);

  return (uint16_t)i32_corrected_value;
}
// ==========================| Internal functions |===========================
